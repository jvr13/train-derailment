import cv2
import numpy as np

cap = cv2.VideoCapture('6sec.mp4')


def region_of_interest(img, vertices):
    mask = np.zeros_like(img)
    #channel_count = img.shape[2]
    match_mask_color = 255
    cv2.fillPoly(mask, vertices, match_mask_color)
    masked_image = cv2.bitwise_and(img, mask)
    return masked_image

def draw_the_lines(img, lines):
    img = np.copy(img)
    blank_image = np.zeros((img.shape[0], img.shape[1], 3), dtype=np.uint8)


    img = cv2.addWeighted(img, 0.8, blank_image, 1, 0.0)
    return img

def process(image):
    region_of_interest_vertices = [
        (920,695), #left
        (950, 250), #middle
        (1580, 695) #right



    ]

    # print(region_of_interest_vertices)
    gray_image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    gray_image = cv2.medianBlur(gray_image, 5)
    canny_image = cv2.Canny(gray_image, 90, 200)
    cropped_image = region_of_interest(canny_image,
                    np.array([region_of_interest_vertices], np.int32),)
    lines = cv2.HoughLinesP(cropped_image,
                            rho=8,
                            theta=np.pi/180,
                            threshold=175,
                            lines=np.array([]),
                            minLineLength=200,
                            maxLineGap=160)
    if lines is None:
        image = cv2.circle(frame, (100, 100), 25, (0, 0, 255), 25)
        return image
    else:
        image_with_lines = draw_the_lines(image, lines)
        image_with_lines = cv2.circle(image_with_lines, (100, 100), 25, (0, 255, 0), 25)
        return image_with_lines


    # if lines is None:
    #     pass
    # elif lines>4:
    #     image_with_lines = draw_the_lines(image, lines)
    #     image_with_lines = cv2.circle(image_with_lines, (100, 100), 25, (0, 255, 0), 25)
    #     return image_with_lines
    # elif lines<4:
    #     image = cv2.circle(frame, (100, 100), 25, (0, 0, 255), 25)
    #     return image

    # if len(lines) >  2:
    #     image_with_lines = drow_the_lines(image, lines)
    #     image_with_lines = cv2.circle(image_with_lines, (100, 100), 25, (0, 255, 0), 25)
    #     return image_with_lines
    #
    # else:
    #     image = cv2.circle(frame, (100, 100), 25, (0, 0, 255), 25)
    #     return image

while cap.isOpened():
    ret, frame = cap.read()
    frame = process(frame)
    cv2.imshow('frame', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()